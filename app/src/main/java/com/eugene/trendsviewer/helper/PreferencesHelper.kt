package com.eugene.trendsviewer.helper

import android.content.Context
import android.content.SharedPreferences
import com.eugene.trendsviewer.TrendsViewerApplication

class PreferencesHelper(androidContext: Context) {

    private val prefs: SharedPreferences =
        androidContext.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE)


    companion object {
        private val SHARED_PREFERENCES_NAME = "${TrendsViewerApplication.getApplicationName()}Prefs"
    }
}