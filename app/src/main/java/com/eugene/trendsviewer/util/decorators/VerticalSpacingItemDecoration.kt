package com.eugene.trendsviewer.util.decorators

import android.graphics.Rect
import android.view.View

class VerticalSpacingItemDecoration(private val verticalSpaceHeight: Int) :
    androidx.recyclerview.widget.RecyclerView.ItemDecoration() {

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: androidx.recyclerview.widget.RecyclerView,
        state: androidx.recyclerview.widget.RecyclerView.State
    ) {
        if (parent.getChildAdapterPosition(view) != parent.adapter?.itemCount ?: 0 - 1) {
            outRect.bottom = verticalSpaceHeight
        }
    }
}
