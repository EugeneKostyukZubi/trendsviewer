package com.eugene.trendsviewer.util.decorators

import android.content.Context
import android.content.res.Resources
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.view.View
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import java.util.*

/**
 * Divider decoration with custom drawable for data recycler view lists
 */

/**
 *
 * @param context Context
 * @param drawableRes resource drawable to use as separator
 * @param showLast whether to show or not the separator after the last element
 */
class DrawableDividerItemDecoration(
    context: Context,
    @DrawableRes drawableRes: Int,
    private val showLast: Boolean
) : androidx.recyclerview.widget.RecyclerView.ItemDecoration() {

    private val mDivider: Drawable?

    init {
        if (drawableRes == 0)
            throw Resources.NotFoundException(
                String.format(Locale.US, "drawableRes %s not found!", drawableRes)
            )
        mDivider = ContextCompat.getDrawable(context, drawableRes)
    }

    override fun onDrawOver(c: Canvas, parent: androidx.recyclerview.widget.RecyclerView, state: androidx.recyclerview.widget.RecyclerView.State) {
        mDivider?.let { dividerDrawable ->
            val left = parent.paddingLeft
            val right = parent.width - parent.paddingRight

            val childCount = parent.childCount
            for (i in 0 until childCount) {
                val child = parent.getChildAt(i)
                if (showLast) {
                    drawDivider(child, c, dividerDrawable, left, right)
                } else {
                    if (parent.getChildAdapterPosition(child) != parent.adapter?.itemCount ?: 0 - 1) {
                        drawDivider(child, c, dividerDrawable, left, right)
                    }
                }
            }
        }
    }

    private fun drawDivider(child: View, c: Canvas, mDivider: Drawable, left: Int, right: Int) {
        val params = child.layoutParams as androidx.recyclerview.widget.RecyclerView.LayoutParams

        val top = child.bottom + params.bottomMargin
        val bottom = top + mDivider.intrinsicHeight

        mDivider.setBounds(left, top, right, bottom)
        mDivider.draw(c)
    }
}
