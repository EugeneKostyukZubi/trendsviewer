package com.eugene.trendsviewer.util

import android.content.Context
import android.graphics.Point
import android.util.DisplayMetrics
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.annotation.DimenRes
import com.eugene.trendsviewer.R
import java.math.BigDecimal
import java.math.RoundingMode

object Utils {

    /**
     * Set margins for a view
     *
     * @param v View to set margins for
     * @param l left margin
     * @param t top margin
     * @param r right margin
     * @param b bottom margin
     */
    fun setMargins(v: View, l: Int, t: Int, r: Int, b: Int) {
        if (v.layoutParams is ViewGroup.MarginLayoutParams) {
            val p = v.layoutParams as ViewGroup.MarginLayoutParams
            p.setMargins(l, t, r, b)
            v.requestLayout()
        }
    }

    /**
     * Add margins to existent values
     *
     * @param v View to update margins for
     * @param l left margin
     * @param t top margin
     * @param r right margin
     * @param b bottom margin
     */
    fun updateMargins(v: View, l: Int, t: Int, r: Int, b: Int) {
        if (v.layoutParams is ViewGroup.MarginLayoutParams) {
            val p = v.layoutParams as ViewGroup.MarginLayoutParams
            p.setMargins(
                    p.leftMargin + l,
                    p.topMargin + t,
                    p.rightMargin + r,
                    p.bottomMargin + b)
            v.requestLayout()
        }
    }

    fun loadDimenPx(context: Context?, @DimenRes res: Int): Int {
        if (context != null) {
            val resources = context.resources
            if (resources != null)
                return resources.getDimension(res).toInt()
        }
        return 0
    }

    fun convertDpToPixel(dp: Float, context: Context): Float {
        val resources = context.resources
        val metrics = resources.displayMetrics
        return dp * (metrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
    }

    fun convertPixelsToDp(px: Float, context: Context): Float {
        val resources = context.resources
        val metrics = resources.displayMetrics
        return px / (metrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
    }

    /**
     * Rounds double value with given precision
     *
     * @param value  value to round
     * @param places items after coma (precision)
     * @return rounded value
     */
    fun round(value: Double, places: Int): Double {
        if (places < 0) throw IllegalArgumentException()

        var bd = BigDecimal(value)
        bd = bd.setScale(places, RoundingMode.HALF_UP)
        return bd.toDouble()
    }

    fun getScreenWidth(context: Context): Int {
        val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = wm.defaultDisplay
        val size = Point()
        display.getSize(size)
        return size.x
    }

    fun isTablet(context: Context): Boolean {
        return context.resources.getBoolean(R.bool.isTablet)
    }
}