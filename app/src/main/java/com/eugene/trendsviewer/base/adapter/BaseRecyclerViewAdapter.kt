package com.eugene.trendsviewer.base.adapter

import android.content.Context
import android.view.LayoutInflater

abstract class BaseRecyclerViewAdapter<M, VH : androidx.recyclerview.widget.RecyclerView.ViewHolder>(protected val context: Context, private var mObjects: MutableList<M>?) : androidx.recyclerview.widget.RecyclerView.Adapter<VH>() {

    protected val mInflater: LayoutInflater = LayoutInflater.from(context)

    @JvmOverloads
    fun setItems(objects: MutableList<M>?, notifyDataSetChanged: Boolean = true) {
        if (mObjects != null) {
            mObjects!!.clear()
            if (objects != null)
                mObjects!!.addAll(objects)
        } else {
            mObjects = objects
        }
        try {
            if (notifyDataSetChanged)
                notifyDataSetChanged()
        } catch (ignore: IllegalStateException) {

        }

    }

    override fun getItemCount(): Int {
        return if (mObjects != null) mObjects!!.size else 0
    }

    fun getItem(position: Int): M? {
        return if (mObjects != null) {
            if (mObjects!!.size > position) mObjects!![position] else null
        } else null
    }
}
