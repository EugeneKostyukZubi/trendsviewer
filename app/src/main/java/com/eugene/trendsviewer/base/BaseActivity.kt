package com.eugene.trendsviewer.base

import android.annotation.SuppressLint
import android.content.res.Resources
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.annotation.IdRes
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.eugene.trendsviewer.R

@SuppressLint("Registered")
open class BaseActivity : AppCompatActivity(), FragmentManagerTransaction {

    private var progressBar : ProgressBar? = null
    private var progressBarIsShowing = false

    companion object {
        const val fragmentContainerId = R.id.container // default fragment container id
        const val progressBarId = R.id.progressBar // default fragment container id
    }

    override fun setContentView(layoutResID: Int) {
        super.setContentView(layoutResID)
        initProgressBar()
    }

    override fun setContentView(view: View?) {
        super.setContentView(view)
        initProgressBar()
    }

    override fun setContentView(view: View?, params: ViewGroup.LayoutParams?) {
        super.setContentView(view, params)
        initProgressBar()
    }

    /**
     * Replaces fragment into default container
     *
     * @param fragment Fragment for changing
     * @param addToBackStack add to history of change
     * @param anim change with animation
     * */
    override fun replaceActivityFragment(fragment: Fragment, addToBackStack: Boolean, anim: Boolean) {
        replaceFragment(fragment, fragmentContainerId, fragment.javaClass.simpleName, addToBackStack, anim)
    }

    /**
     * Replaces fragment into custom container
     *
     * @param fragment Fragment for changing
     * @param container custom container id
     * @param addToBackStack add to history of change
     * @param anim change with animation
     * */
    override fun replaceActivityFragment(fragment: Fragment, container: Int, addToBackStack: Boolean, anim: Boolean) {
        replaceFragment(fragment, container, fragment.javaClass.simpleName, addToBackStack, anim)
    }

    /**
     * Replaces fragment into custom container
     *
     * @param fragment Fragment for changing
     * @param container custom container id
     * @param tag identifier of fragment
     * @param addToBackStack add to history of change
     * @param anim change with animation
     * */
    override fun replaceActivityFragment(fragment: Fragment, container: Int, tag: String, addToBackStack: Boolean, anim: Boolean) {
        replaceFragment(fragment, container, tag, addToBackStack, anim)
    }

    /**
     * Replaces fragment into default container
     *
     * @param fragment Fragment for changing
     * @param tag identifier of fragment
     * @param addToBackStack add to history of change
     * @param anim change with animation
     * */
    override fun replaceActivityFragment(fragment: Fragment, tag: String, addToBackStack: Boolean, anim: Boolean) {
        replaceFragment(fragment, fragmentContainerId, tag, addToBackStack, anim)
    }

    /**
     * Create Toast.makeText with data form arguments
     *
     * @param res string from resource
     * @param duration Duration of show of Toast
     * */
    fun showToast(@StringRes res: Int, duration: Int) {
        try {
            Toast.makeText(this, res, duration).show()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * Create Toast.makeText with data form arguments
     *
     * @param res string from resource
     * */
    fun showToast(@StringRes res : Int) {
        showToast(res, Toast.LENGTH_LONG)
    }

    /**
     * Create Toast.makeText with data form arguments
     *
     * @param string message for showing
     * @param duration Duration of show of Toast
     * */
    fun showToast(string : String, duration: Int) {
        try {
            Toast.makeText(this, string, duration).show()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * Create Toast.makeText with data form arguments
     *
     * @param string message for showing
     * */
    fun showToast(string : String) {
        showToast(string, Toast.LENGTH_LONG)
    }

    /**
     * Show progress bar if it did not showed
     * */
    fun showProgressBar() {
        progressBar?.let { notNullProgressBar ->
            if(!progressBarIsShowing) {
                notNullProgressBar.visibility = View.VISIBLE
                progressBarIsShowing = true
            }
        }
    }

    /**
     * Hide progress bar if it showed
     * */
    fun hideProgressBar() {
        progressBar?.let { notNullProgressBar ->
            if(progressBarIsShowing) {
                notNullProgressBar.visibility = View.GONE
                progressBarIsShowing = false
            }
        }
    }

    private fun replaceFragment(fragment: Fragment, @IdRes container: Int, tag: String, addToBackStack: Boolean, anim: Boolean) {
        if (container == 0) {
            throw Resources.NotFoundException("Fragment id container not found!")
        }
        val fm = supportFragmentManager
        val transaction = fm.beginTransaction()

        if (anim)
            transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
        if (addToBackStack)
            transaction.addToBackStack(tag)

        transaction.replace(container, fragment, tag)
        transaction.commitAllowingStateLoss()
    }

    private fun initProgressBar() {
        try {
            progressBar = findViewById(progressBarId)
            progressBar?.visibility = View.GONE
            progressBarIsShowing = false
        } catch (e : Exception) {
            e.printStackTrace()
        }
    }
}