package com.eugene.trendsviewer.base

import com.eugene.trendsviewer.util.Utils
import android.content.Context
import android.os.Bundle
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment

open class BaseFragment : Fragment() {

    private var fmTransaction: FragmentManagerTransaction? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            fmTransaction = context as FragmentManagerTransaction
        } catch (e: ClassCastException) {
            throw ClassCastException(context.javaClass.simpleName + " must implement " + FragmentManagerTransaction::class.java)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (!Utils.isTablet(activity!!))
            setHasOptionsMenu(true)
        handleArguments(savedInstanceState ?: arguments)
    }

    protected open fun handleArguments(args: Bundle?) {}

    fun replaceActivityFragment(fragment: Fragment, addToBackStack: Boolean, anim: Boolean) {
        fmTransaction?.replaceActivityFragment(fragment, addToBackStack, anim)
    }

    fun replaceActivityFragment(
        fragment: Fragment,
        container: Int,
        addToBackStack: Boolean,
        anim: Boolean) {
        fmTransaction?.replaceActivityFragment(fragment, container, addToBackStack, anim)
    }

    fun replaceActivityFragment(
        fragment: Fragment,
        container: Int,
        tag: String,
        addToBackStack: Boolean,
        anim: Boolean) {
        fmTransaction?.replaceActivityFragment(fragment, container, tag, addToBackStack, anim)
    }

    fun replaceActivityFragment(
        fragment: Fragment,
        tag: String,
        addToBackStack: Boolean,
        anim: Boolean) {
        fmTransaction?.replaceActivityFragment(fragment, tag, addToBackStack, anim)
    }

    protected fun showProgressDialog() {
        (requireActivity() as? BaseActivity)?.showProgressBar()
    }

    protected fun hideProgressDialog() {
        (requireActivity() as? BaseActivity)?.hideProgressBar()
    }

    protected fun showToast(@StringRes res: Int) {
        (requireActivity() as? BaseActivity)?.showToast(res)
    }

    override fun onDetach() {
        super.onDetach()
        fmTransaction = null
    }
}