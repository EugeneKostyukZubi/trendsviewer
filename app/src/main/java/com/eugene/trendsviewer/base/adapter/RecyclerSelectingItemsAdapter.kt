package com.eugene.trendsviewer.base.adapter

import android.content.Context
import android.os.Handler
import android.util.SparseBooleanArray
import java.util.*

abstract class RecyclerSelectingItemsAdapter<M, VH : androidx.recyclerview.widget.RecyclerView.ViewHolder> :
    RecyclerViewAdapter<M, VH> {

    enum class ChooseMode {
        NONE, SINGLE, MULTI
    }

    private val mHandler = Handler()
    private val selectedItems = SparseBooleanArray()
    private var mChooseMode = ChooseMode.NONE
    private var onSelectionUpdateListener: OnSelectionUpdateListener? = null

    val selectedItemCount: Int
        get() = if (mChooseMode == ChooseMode.NONE) 0 else selectedItems.size()

    val selectedItemsPositions: List<Int>?
        get() {
            if (mChooseMode == ChooseMode.NONE) return null

            val items = ArrayList<Int>(selectedItems.size())
            for (i in 0 until selectedItems.size()) {
                items.add(selectedItems.keyAt(i))
            }
            return items
        }

    constructor(context: Context) : super(context) {}

    constructor(context: Context, objects: MutableList<M>) : super(context, objects) {}

    fun setOnSelectionUpdateListener(onSelectionUpdateListener: OnSelectionUpdateListener) {
        this.onSelectionUpdateListener = onSelectionUpdateListener
    }

    fun setChooseMode(chooseMode: ChooseMode) {
        //clear all selected items before set new Choose Mode
        clearSelections()

        this.mChooseMode = chooseMode
    }

    protected fun isSelectedItem(position: Int): Boolean {
        return mChooseMode != ChooseMode.NONE && selectedItems.get(position, false)

    }

    fun toggleItem(pos: Int) {
        when (mChooseMode) {
            ChooseMode.SINGLE -> {
                if (selectedItems.get(pos, false)) {
                    selectedItems.delete(pos)
                } else {
                    clearSelections()
                    selectedItems.put(pos, true)
                }
                safeNotifyItemChanged(pos)
                this.onSelectionUpdateListener?.onToggleItem(pos, selectedItemCount)
            }
            ChooseMode.MULTI -> {
                if (selectedItems.get(pos, false)) {
                    selectedItems.delete(pos)
                } else {
                    selectedItems.put(pos, true)
                }
                safeNotifyItemChanged(pos)
                this.onSelectionUpdateListener?.onToggleItem(pos, selectedItemCount)
            }
            else -> {
                // nothing
            }
        }
    }

    fun clearSelections() {
        if (mChooseMode == ChooseMode.NONE) return

        selectedItems.clear()
        safeNotifyDataSetChanged()
    }

    fun getSelectedItems(): List<M>? {
        if (mChooseMode == ChooseMode.NONE) return null

        val items = ArrayList<M>(selectedItems.size())
        for (i in 0 until selectedItems.size()) {
            val position = selectedItems.keyAt(i)
            val item = getItem(position)
            items.add(item!!)
        }
        return items
    }

    protected fun safeNotifyItemChanged(position: Int) {
        mHandler.post {
            try {
                notifyItemChanged(position)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    protected fun safeNotifyDataSetChanged() {
        mHandler.post {
            try {
                notifyDataSetChanged()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
}
