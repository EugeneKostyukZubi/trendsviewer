package com.eugene.trendsviewer.base

import androidx.annotation.IdRes
import androidx.fragment.app.Fragment

interface FragmentManagerTransaction {

    fun replaceActivityFragment(fragment: Fragment, addToBackStack: Boolean, anim: Boolean)

    fun replaceActivityFragment(fragment: Fragment, container: Int, addToBackStack: Boolean, anim: Boolean)

    fun replaceActivityFragment(fragment: Fragment, @IdRes container: Int, tag: String, addToBackStack: Boolean, anim: Boolean)

    fun replaceActivityFragment(fragment: Fragment, tag: String, addToBackStack: Boolean, anim: Boolean)
}