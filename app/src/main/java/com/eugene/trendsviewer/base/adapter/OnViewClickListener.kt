package com.eugene.trendsviewer.base.adapter

import android.view.View

interface OnViewClickListener {
    fun onViewClick(v: View, position: Int, id: Long)
}
