package com.eugene.trendsviewer.base.adapter

import android.view.View

interface OnItemLongClickListener {
    fun onItemLongClick(v: View, position: Int, id: Long): Boolean
}
