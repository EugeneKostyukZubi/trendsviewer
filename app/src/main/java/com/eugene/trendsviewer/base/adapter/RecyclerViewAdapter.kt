package com.eugene.trendsviewer.base.adapter

import android.content.Context
import android.view.View
import java.util.*

abstract class RecyclerViewAdapter<M, VH : androidx.recyclerview.widget.RecyclerView.ViewHolder> : BaseRecyclerViewAdapter<M, VH>, View.OnClickListener, View.OnLongClickListener {

    private var mOnViewClickListener: OnViewClickListener? = null
    private var mOnItemClickListener: OnItemClickListener? = null
    private var mOnItemLongClickListener: OnItemLongClickListener? = null

    var isClicksEnabled = true

    constructor(context: Context) : super(context, ArrayList<M>()) {
        this.setHasStableIds(true)
    }

    constructor(context: Context, list: MutableList<M>) : super(context, list) {
        this.setHasStableIds(true)
    }

    fun setOnViewClickListener(onViewClickListener: OnViewClickListener?) {
        this.mOnViewClickListener = onViewClickListener
    }

    fun setOnItemClickListener(onItemClickListener: OnItemClickListener?) {
        mOnItemClickListener = onItemClickListener
    }

    fun setOnItemLongClickListener(onItemLongClickListener: OnItemLongClickListener?) {
        mOnItemLongClickListener = onItemLongClickListener
    }

    @Suppress("UNCHECKED_CAST")
    override fun onClick(v: View) {
        if (!isClicksEnabled) return

        val vh = v.tag as VH

        if (v.parent !is androidx.recyclerview.widget.RecyclerView && mOnViewClickListener != null) {
            mOnViewClickListener!!.onViewClick(v, vh.layoutPosition, vh.itemId)
        } else if (mOnItemClickListener != null) {
            mOnItemClickListener!!.onItemClick(v, vh.layoutPosition, vh.itemId)
        }
    }

    @Suppress("UNCHECKED_CAST")
    override fun onLongClick(v: View): Boolean {
        if (!isClicksEnabled) return false

        val vh = v.tag as VH
        return (mOnItemLongClickListener != null && mOnItemLongClickListener!!.onItemLongClick(v, vh.layoutPosition, vh.itemId))
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }
}
