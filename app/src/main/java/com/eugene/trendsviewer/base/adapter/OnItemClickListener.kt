package com.eugene.trendsviewer.base.adapter

import android.view.View

interface OnItemClickListener {
    fun onItemClick(v: View, position: Int, id: Long)
}
