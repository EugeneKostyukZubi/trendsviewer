package com.eugene.trendsviewer.base.adapter


interface OnSelectionUpdateListener {
    fun onToggleItem(position: Int, selectedCount: Int)
}
