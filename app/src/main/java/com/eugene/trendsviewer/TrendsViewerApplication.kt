package com.eugene.trendsviewer

import android.annotation.SuppressLint
import android.app.Application

@SuppressLint("Registered")
class TrendsViewerApplication : Application() {

    companion object {
        private var applicationName : String? = null
        fun getApplicationName() = applicationName!!
    }

    override fun onCreate() {
        super.onCreate()
        applicationName = getString(R.string.app_name)
    }
}